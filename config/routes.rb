Rails.application.routes.draw do
  root :to => 'occurrences#charts'
  get 'developer' => 'developer#index'
  post 'developer' => 'developer#index'
  get 'occurrences/charts' => 'occurrences#charts'
  get 'occurrences/maps' => 'occurrences#maps'
  post 'occurrences/maps' => 'occurrences#maps'
  get 'occurrences' => 'occurrences#index'
end

namespace :crawler do
  desc "crawl data from http://www.ssp.sp.gov.br/novaestatistica/Pesquisa.aspx"
  task :run => :environment do
    puts "#{Time.now} - Crawler running"
    getter = GetData.new
    getter.get_city_data
    puts "#{Time.now} - City updated"
    getter.get_districts_data
    puts "#{Time.now} - Districts updated"
    getter.destroy
    puts "#{Time.now} - Crawler finished"
  end
end

class DeveloperController < ApplicationController

  def index
    @running = 0
    if params[:starter_year] != nil and params[:finish_year] != nil and params[:starter_district]!=nil
      @running = 1
      getter = GetData.new
      getter.get_city_data params[:starter_year]..params[:finish_year]
      getter.get_districts_data params[:starter_year]..params[:finish_year], params[:starter_district]..params[:finish_district]
      getter.destroy
      @running = 2
    end
  end
end

module KmlHelper
  def save_kml kml, file_name
    open("#{file_name}.kml", 'w') do |f|
      f.puts kml
    end
  end

  def zip_kml file_name
    system("zip -j #{file_name}.zip #{file_name}.kml")
    File.rename("#{file_name}.zip", "#{file_name}.kmz")
  end
end
require_relative 'chart/bar_chart'
require_relative 'chart/pie_chart'
require_relative 'chart/line_chart'
require_relative 'chart/column_chart'

class ChartFactory
  def self.get_chart district, crime, month, year
    if district.to_s.empty?
      BarChart.new crime, month, year
    elsif crime.nil? or crime.empty?
      PieChart.new district, month, year
    elsif month.nil? or month.empty? or year.nil? or year.empty?
      LineChart.new district, crime, month, year
    else
      ColumnChart.new district, crime, month, year
    end
  end
end
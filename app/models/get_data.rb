#encoding: utf-8
require 'csv'
require 'connection_pool'  
class GetData

  FIRST = ['Mensal', 'ProdutividadePolicial']
  SECOND = ['Município', 'Delegacia']

  def initialize    
    @headless = Headless.new
    @headless.start
    @browser_pool = ConnectionPool.new(size: 5, timeout: 200) { BrowserFactory.create_browser }
  end

  def destroy
    @browser_pool.shutdown {|b| b.close}
    @browser_pool = nil
    @headless.destroy
  end

  def get_city_data year_range=2013..Time.now.year
    raise 'Browser pool is empty' if @browser_pool.nil? 
    @browser_pool.with do |browser|
      DownloadCSV.set_district browser, -1
      district = District.get_city
      get_district_data browser, district, year_range
    end
  end

  # fill the district table with the districts occurrences
  def get_districts_data year_range=2013..Time.now.year, district_range=0..93
    raise 'Browser pool is empty' if @browser_pool.nil? 
    Parallel.each((district_range).to_a, in_threads: 5) do |i|
      @browser_pool.with do |browser|
        d_name = DownloadCSV.set_district browser, i
        district = District.get_district d_name
        get_district_data browser, district, year_range
      end
    end
  end

  def get_district_data browser, district, year_range
    unless district.nil?
      for year in year_range
        if !Occurrence.completed? district, year
          get_district_year_data browser, district, year
          puts "#{Time.now} - District: #{district.name}, Year: #{year} updated"
        else
          puts "#{Time.now} - District: #{district.name}, Year: #{year} skipped"
        end
      end
    end
  end

  def get_district_year_data browser, district, year
    FileUtils.rm_rf(Dir.glob(browser.download_path))
    DownloadCSV.download_csv browser, year
    ActiveRecord::Base.connection_pool.with_connection do
      ActiveRecord::Base.transaction do
        FIRST.each do |first|
          path = browser.download_path + first + '-' + SECOND[district.number > 0 ? 1 : 0] 
          data = get_csv_file_data browser, path  
          fill_database district, year, data
        end
      end
    end
  end

  def get_csv_file_data browser, path    
    data = []
    until data.count > 0
        system("iconv -f UCS-2 -t UTF-8 #{path} > #{browser.download_path}output.csv")
        data = CSV.read("#{browser.download_path}output.csv", {:col_sep => ';'})
    end
    data
  end

  def fill_database district, year, data
    for row in 2..(data.count-1)
      unless data[row][0].nil?
        type = CrimeType.get_crime_type data[row][0][/([^(]+)/].strip
        for col in 1..12
          Occurrence.create(district: district,
                            crime_type: type,
                            year: year,
                            month: col,
                            value: data[row][col].delete('.').to_i) unless data[row][col] == '...'
        end
      end
    end
  end
end




class Chart

  attr_reader :title
  attr_reader :data
  attr_reader :type

  def initialize district, crime, month, year
    set_title district, crime, month, year
  end

  def set_title district, crime, month, year  
    @title = ''
    if district.to_s.empty?
      @title += 'Estatística por Distrito'
    else
      @title += 'Estatística para ' + District.find(district).name
    end
    @title += ' em' unless month.to_s.empty? or year.to_s.empty?
    @title += ' ' + Date::MONTHNAMES[month.to_i].to_s unless month.to_s.empty?
    @title += ' ' + year unless year.to_s.empty?
    @title += ' (' + CrimeType.find(crime).name + ')' unless crime.to_s.empty?
  end
end
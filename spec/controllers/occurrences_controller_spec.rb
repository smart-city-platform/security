require 'rails_helper'

RSpec.describe OccurrencesController do
  describe "GET #charts" do
    it "should get data from database to setup dropdowns on charts page" do     
      occurrence = FactoryGirl.create(:occurrence, :city)
      crime_type = occurrence.crime_type
      district = occurrence.district
      year = occurrence.year
      months = (1..12).map {|m| [Date::MONTHNAMES[m], m]}
      get :charts
      expect(assigns(:district_list)).to eq([district])
      expect(assigns(:crime_type_list)).to eq([crime_type])
      expect(assigns(:month_list)).to eq(months)      
      expect(assigns(:year_list)).to eq([[year, year]])
    end
  end

  describe "GET #maps" do
    it "should get data from database to setup dropdowns on maps page" do     
      occurrence = FactoryGirl.create(:occurrence, :city)
      crime_type = occurrence.crime_type
      district = occurrence.district
      year = occurrence.year
      months = (1..12).map {|m| [Date::MONTHNAMES[m], m]}
      get :charts
      expect(assigns(:district_list)).to eq([district])
      expect(assigns(:crime_type_list)).to eq([crime_type])
      expect(assigns(:month_list)).to eq(months)      
      expect(assigns(:year_list)).to eq([[year, year]])
    end
  end

end
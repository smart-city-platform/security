require 'rails_helper'

RSpec.describe ChartFactory, :type => :model do  

  describe ".get_chart" do
    let!(:occurrence) { FactoryGirl.create(:occurrence, :dp) }
    let!(:occurrence_c) { FactoryGirl.create(:occurrence, :city) }

    context "with all districts selected" do
      it "should create a bar chart" do
        chart = ChartFactory.get_chart '', '', '', ''
        expect(chart).to_not be_nil
        expect(chart.data).to_not be_empty
        expect(chart.type).to be :bar
      end
    end

    context "with one district selected and all crimes selected" do
      it "should create a pie chart" do
        chart = ChartFactory.get_chart occurrence.district.id.to_s, '', '', ''
        expect(chart).to_not be_nil
        expect(chart.data).to_not be_empty
        expect(chart.type).to be :pie
      end
    end

    context "with one district selected and one crime selected and all monthes selected" do
      it "should create a line chart" do
        chart = ChartFactory.get_chart occurrence.district.id.to_s, occurrence.crime_type.id.to_s, '', ''
        expect(chart).to_not be_nil
        expect(chart.data).to_not be_empty
        expect(chart.type).to be :line
      end
    end

    context "with one district selected and one crime selected and all years selected" do
      it "should create a line chart" do
        chart = ChartFactory.get_chart occurrence.district.id.to_s, occurrence.crime_type.id.to_s, occurrence.month.to_s, ''
        expect(chart).to_not be_nil
        expect(chart.data).to_not be_empty
        expect(chart.type).to be :line
      end
    end

    context "with all selects with value" do
      it "should create a column chart" do
        chart = ChartFactory.get_chart occurrence.district.id.to_s, occurrence.crime_type.id.to_s, occurrence.month.to_s, occurrence.year.to_s
        expect(chart).to_not be_nil
        expect(chart.data).to_not be_empty
        expect(chart.type).to be :column
      end
    end
  end

end
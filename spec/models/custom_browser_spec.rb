require 'rails_helper'

RSpec.describe CustomBrowser, :type => :model do

  before(:all) do
    @headless = Headless.new
    @headless.start
    @custom_browser = BrowserFactory.create_browser
  end

  it "is a valid browser" do
    expect(@custom_browser).to_not be_nil
  end

  it "has a public download_path" do
    expect(@custom_browser).to respond_to :download_path
  end

  it "quacks like a Watir::Browser" do
    watir_browser = Watir::Browser.new(:firefox)
    watir_browser.methods.each{ |method|
      expect(@custom_browser).to respond_to method
    }
    watir_browser.close
  end

  after(:all) do
    @custom_browser.close
    @headless.destroy
  end

end
require 'rails_helper'

RSpec.describe CrimeType, :type => :model do

  subject(:crime_type) { FactoryGirl.create(:crime_type) }

  it "has a valid factory" do
    expect(crime_type).to be_valid
  end

  it { is_expected.to validate_presence_of(:name)}
  it { is_expected.to have_many(:occurrences)}

  describe ".get_crime_type" do

    context "exists in database" do
      let(:name) { crime_type.name }

      it "is equal" do
        ctype = CrimeType.get_crime_type name
        expect(crime_type.id).to eql ctype.id
      end
    end

    context "doesn't exists in database" do
      let(:name) { crime_type.name + "POR ACIDENTE DE TRÂNSITO" }

      it "is not equal" do
        new_crime_type = CrimeType.get_crime_type name
        expect(new_crime_type.id).to_not eql crime_type.id
      end
    end
  end
end
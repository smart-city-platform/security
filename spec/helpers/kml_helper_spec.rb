require 'rails_helper'
RSpec.describe KmlHelper do  

  let(:test_path) {Rails.root.join("spec").join("files")}

  describe '#save_kml' do
    let(:file_name) {test_path.join("save_test").to_s}
    let(:text) {"IT'S A KML"}
    it 'should create a file with the kml text' do
      helper.save_kml text, file_name
      expect(File).to exist("#{file_name}.kml")
    end
  end

  describe '#zip_kml' do
    let(:file_name) {test_path.join("zip_test").to_s}
    it 'should compress a file and create a kmz file' do
      File.open("#{file_name}.kml", "w") { |file| file.write("SOMETHING") }
      helper.zip_kml file_name
      expect(File).to exist("#{file_name}.kmz")
      expect(File).not_to exist("#{file_name}.zip")
    end
  end

  after(:all) do
    Dir.foreach(Rails.root.join("spec").join("files").to_s) do |f|
      fn = File.join(Rails.root.join("spec").join("files").to_s, f)
      File.delete(fn) unless /^\..*/ =~ f 
    end
  end
end
class CreateOccurrence < ActiveRecord::Migration

  def change
    create_table :crime_types do |t|
      t.string :name, :unique => true
    end

    create_table :occurrences do |t|
      t.references :district
      t.integer :year
      t.integer :month
      t.references :crime_type
      t.integer :value
    end
  end
  
end

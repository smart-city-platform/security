class AddOccurrenceUniqueConstraint < ActiveRecord::Migration
  def change
    add_index :occurrences, [:district_id, :crime_type_id, :year, :month], unique: true, name: "occurrence_unique_index"
  end
end
